from uuid import UUID


def _validate_uuid4(uuid_string):
    """
    Validate that a UUID string is in
    fact a valid uuid4.
    """
    try:
        val = UUID(uuid_string, version=4)
    except ValueError:
        # If it's a value error, then the string
        # is not a valid hex code for a UUID.
        return False
    return str(val).lower() == uuid_string.lower()


class InvoiceId(str):
    @staticmethod
    def is_valid_invoice_id(invoiceId: str) -> bool:
        return _validate_uuid4(invoiceId)

import typing
import functools
from datetime import datetime, timedelta
import json
from logging import getLogger
from django.conf import settings
from grapheneapi.exceptions import (NumRetriesReached, RPCError, RPCRequestError)
from vinchainio.exceptions import AccountDoesNotExistsException

from vinchainio.memo import Memo
from vinchainio.account import Account

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, query, Q, DocType, utils

from .chain import get_blockchain
from .invoice import InvoiceId
from .payment_system_provider import AccountPaymentData
from .types import RetrievingPaymentDataError


_logger = getLogger('monitoring')
_LOG_SOURCE = 'xxxxxxxx'
_logger_extra = {
    'source': _LOG_SOURCE,
}

timeformat = '%Y-%m-%dT%H:%M:%S'


_account_name_to_id_cache = {}
_account_id_to_name_cache = {}


def vinchain_network_exception_handler(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        def exception_handler():
            _logger.exception('%s: Exception of blockchain connection!!!',
                          _LOG_SOURCE, extra=_logger_extra)
            raise RetrievingPaymentDataError
        try:
            result = func(*args, **kwargs)
        except (NumRetriesReached, RPCError, RPCRequestError) as e:
            exception_handler()
        except BaseException as e:
            if e.args[0] == 'exceptions must derive from BaseException':
                exception_handler()
            else:
                raise
        return result

    return wrapper




def clear_caches():
    global _account_name_to_id_cache, _account_id_to_name_cache
    _account_name_to_id_cache = {}
    _account_id_to_name_cache = {}


@vinchain_network_exception_handler
def get_account_payment_data(account, from_date=None, to_date=None):
    payment_data = {}

    account_id = _account_name_to_id(account)

    es_connect_args = {'hosts': [settings.VINCHAIN_ELASTICSEARCH_NODE], 'timeout': 60}

    data = _get_vinchain_account_history(es_connect_args, account, '0', from_date, to_date)
    if not data:
        return payment_data

    blockchain = get_blockchain()
    if blockchain.wallet.locked():
        blockchain.wallet.unlock(settings.VINCHAIN_WALLET_PASSWORD)
    memo = Memo(vinchain_instance=blockchain)
    for operation in data:
        op = json.loads(operation['operation_history']['op'])
        if len(op) <= 1:
            continue
        op = op[1]
        from_ = op['from']
        to = op['to']
        if to != account_id:
            continue
        amount = op['amount']['amount']/1000000.0
        asset_id = op['amount']['asset_id']
        memo_data = op['memo']
        memo_str = memo.decrypt(memo_data)
        time = datetime.strptime(operation['block_data']['block_time'], '%Y-%m-%dT%H:%M:%S')
        if InvoiceId.is_valid_invoice_id(memo_str):
            invoiceId = InvoiceId(memo_str)
            payment_data[invoiceId] = AccountPaymentData(from_=from_, to=to, amount=amount, asset_id=asset_id,
                                                        invoice_id=invoiceId, operation_time=time, extended_data='')

    return payment_data


@vinchain_network_exception_handler
def get_invoice_data(account_id, from_date, to_date, invoice_id) -> AccountPaymentData:
    data = get_account_payment_data(account_id, from_date, to_date)
    return data.get(invoice_id)


@vinchain_network_exception_handler
def check_sign(sign, account, max_time_diff_sec=5 * 60):
    blockchain = get_blockchain()
    if blockchain.wallet.locked():
        blockchain.wallet.unlock(settings.VINCHAIN_WALLET_PASSWORD)
    msg = blockchain.get_message(sign)

    try:
        msg.verify(account=account)
    except Exception:
        _logger.error('%s: Error of verifying of sign %s for account %s !!!.',
                      _LOG_SOURCE, sign, account, extra=_logger_extra)
        return False

    too_old_message = (
        datetime.now() - datetime.strptime(msg.meta['timestamp'], '%Y-%m-%dT%H:%M:%S')
    ) > timedelta(seconds=max_time_diff_sec)
    if too_old_message:
        _logger.error('%s: Sign %s is too old !!!', _LOG_SOURCE, sign, extra=_logger_extra)
        return False

    return True


def _is_id(value):
    """ Returns True if value is ID like (1.2.3)
    """
    if not value or not isinstance(value, str):
        return False
    if not value[0].isdigit():
        return False
    vl = value.split('.')
    return all((v.isdigit for v in vl))


def _get_account_name_and_id(value):
    blockchain = get_blockchain()
    try:
        account = Account(account=value, vinchain_instance=blockchain, lazy=False, full=False)
    except AccountDoesNotExistsException:
        return (None, None)
    return (account.name, account.identifier)


def _account_name_to_id(value):
    if _is_id(value):
        return value

    acccount_id = _account_name_to_id_cache.get(value)

    if acccount_id is None:
        acccount_name, acccount_id =_get_account_name_and_id(value)

        if acccount_name is None:
            return None

        _account_name_to_id_cache[acccount_name] = acccount_id
        _account_id_to_name_cache[acccount_id] = acccount_name

    return acccount_id


def _account_id_to_name(value):
    if not _is_id(value):
        return value

    acccount_name = _account_id_to_name_cache.get(value)

    if acccount_name is None:
        acccount_name, acccount_id =_get_account_name_and_id(value)

        if acccount_name is None:
            return None

        _account_name_to_id_cache[acccount_name] = acccount_id
        _account_id_to_name_cache[acccount_id] = acccount_name

    return acccount_name


def _get_vinchain_account_history(es_connect_args, account_id, operation_type, from_date, to_date):

    try:
        es = Elasticsearch(**es_connect_args)

        s = Search(using=es, index="graphene-*")

        q = Q()
        if account_id and operation_type:
            q = Q("match", account_history__account=account_id) & Q("match", operation_type=operation_type)
        elif account_id and not operation_type:
            q = Q("match", account_history__account=account_id)

        range_g = {}
        if from_date:
            range_g['gte'] = from_date
        if to_date:
            range_g['lte'] = to_date
        range_query = Q("range", block_data__block_time=range_g)
        s.query = q & range_query

        s = s.sort('-block_data.block_time')

         #TODO: add returned fields list to request

        response = s.execute()
        results = []
        if response:
            for hit in response:
                results.append(hit.to_dict())
        else:
            _logger.debug('%s: There is not records in ELS DB for account_id = %s, operation_type = %s, from_date = %s,'
                          ' to_date = %s', _LOG_SOURCE, account_id, operation_type, from_date, to_date,
                          extra=_logger_extra)

        return results

    except Exception as e:
        _logger.error('%s: Exception of retrieving from Elasticsearch DB!!!. Connection args = %s',
                      _LOG_SOURCE, es_connect_args, extra=_logger_extra)
        raise e

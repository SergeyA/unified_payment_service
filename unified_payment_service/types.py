from enum import Enum


class PaymentSystemId(Enum):
    VINCHAIN = 'vinchain'


class RetrievingPaymentDataError(Exception):
    pass

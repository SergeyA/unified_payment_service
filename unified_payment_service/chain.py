from vinchainio import VinChain
from django.conf import settings


def get_blockchain():
    return VinChain(**settings.VINCHAIN_CHAIN_SETTINGS)

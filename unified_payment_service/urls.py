"""unified_payment_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from django.conf import settings
from django.conf.urls.static import static

from unified_payment_service.views import (
    create_invoice,
    get_invoice_info,
    get_invoices_info
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('payment_service/invoices/create/', create_invoice),
    path('payment_service/invoices/get_invoice/', get_invoice_info),
    path('payment_service/invoices/get_invoices/', get_invoices_info),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

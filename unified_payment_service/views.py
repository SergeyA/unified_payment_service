from datetime import (
    datetime,
    timedelta,
)

from logging import getLogger

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST

from marshmallow import fields, Schema

from unified_payment_service.payment_service import create_invoice as ps_create_invoice
from unified_payment_service.payment_service import get_invoice_info as ps_get_invoice_info
from unified_payment_service.payment_service import check_sign
from unified_payment_service.types import PaymentSystemId


_logger = getLogger('monitoring')

timeformat = '%Y-%m-%dT%H:%M:%S'

_LOG_SOURCE = 'payment_service_webservice'


class CreateInvoiceSchema(Schema):
    recipient = fields.String(required=True)
    payment_system_id = fields.String(required=True)
    asset_id = fields.String(required=True)
    amount = fields.Float(required=True)
    operation_details = fields.String(required=False)
    description = fields.String(required=False)
    reserved = fields.String(required=False)
    expiration_timeout = fields.Int(required=False) # timeout of invoice expiration in seconds, 1 day by default
    url_success = fields.String(required=False) # ????
    url_failed = fields.String(required=False)  # ????
    timestamp = fields.Int(required=True)
    sign = fields.String(required=True)


class GetInvoiceInfoSchema(Schema):
    invoice_id = fields.String(required=True)
    sign = fields.String(required=True)


@csrf_exempt
@require_POST
def create_invoice(request):
    schema = CreateInvoiceSchema()
    serialized_body = schema.loads(request.body)

    if serialized_body.errors:
        extra = {
            'source': _LOG_SOURCE,
            'success': False,
            'status_code': 400,
        }
        _logger.error('%s: Deserialization error.', _LOG_SOURCE, extra=extra)
        return JsonResponse({'errors': serialized_body.errors}, status=400)

    body = serialized_body.data

    # TODO: debug
    #max_time_diff_sec = 160 * 60
    max_time_diff_sec = 5*60
    if not check_sign(PaymentSystemId(body['payment_system_id']), body['sign'], body['recipient'], max_time_diff_sec):
        extra = {
            'source': _LOG_SOURCE,
            'success': False,
            'status_code': 403,
        }
        _logger.error('%s: create_invoice(): Credential is not valid.!!!.', _LOG_SOURCE, extra=extra)
        return JsonResponse({'errors': {'signature': 'Your credential is not valid.'}}, status=403)

    try:
        expiration_timeout = body.get('expiration_timeout')
        expiration_timeout = timedelta(seconds=expiration_timeout) if expiration_timeout is not None else None
        invoice_id = ps_create_invoice(body['recipient'],
                                       PaymentSystemId(body['payment_system_id']),
                                       body['asset_id'],
                                       body['amount'],
                                       body.get('operation_details', ''),
                                       datetime.fromtimestamp(body['timestamp']),
                                       body.get('description', ''),
                                       body.get('reserved', ''),
                                       expiration_timeout,
                                       body.get('url_success'),
                                       body.get('url_failed'),
                                       )
    except  Exception as e:
        extra = {
            'source': _LOG_SOURCE,
            'success': False,
            'status_code': 404,
        }
        _logger.exception('%s: Exception while creating invoice!!!.', _LOG_SOURCE, extra=extra)
        return JsonResponse({'errors': 'Exception while creating invoice.'}, status=404)

    extra = {
        'source': _LOG_SOURCE,
        'invoice_id': invoice_id,
        'recepient': body['recipient'],
        'params': body,
        'success': True,
        'status_code': 201,
    }
    _logger.info('%s: Invoice %s for recepient %s has been created successfuly/', _LOG_SOURCE, invoice_id,
                 body['recipient'], extra=extra)
    return JsonResponse(
        {
            "invoice_id": invoice_id,
        },
        status=201
    )


@csrf_exempt
@require_POST
def get_invoice_info(request):
    schema = GetInvoiceInfoSchema()
    serialized_body = schema.loads(request.body)

    if serialized_body.errors:
        extra = {
            'source': _LOG_SOURCE,
            'success': False,
            'status_code': 400,
        }
        _logger.error('%s: Deserialization error.', _LOG_SOURCE, extra=extra)
        return JsonResponse({'errors': serialized_body.errors}, status=400)

    body = serialized_body.data

    try:
        invoice_info = ps_get_invoice_info(body['invoice_id'])
    except  Exception as e:
        extra = {
            'source': _LOG_SOURCE,
            'success': False,
            'status_code': 404,
        }
        _logger.exception('%s: Exception while to retrieve invoice info!!!.', _LOG_SOURCE, extra=extra)
        return JsonResponse({'errors': 'Exception while to retrieve invoice info.'}, status=404)

    if invoice_info is None:
        return JsonResponse({'errors': {'invoice_id': 'Invoice {} does not exists'.format(body['invoice_id'])}},
                            status=405)

    # TODO: debug
    # max_time_diff_sec = 160 * 60
    max_time_diff_sec = 5*60
    if not check_sign(PaymentSystemId(invoice_info.payment_system_id),
                      body['sign'], invoice_info.recipient, max_time_diff_sec):
        extra = {
            'source': _LOG_SOURCE,
            'success': False,
            'status_code': 403,
        }
        _logger.error('%s: create_invoice(): Credential is not valid.!!!.', _LOG_SOURCE, extra=extra)
        return JsonResponse({'errors': {'signature': 'Your credential is not valid.'}}, status=403)


    return JsonResponse({
        'invoice_id': invoice_info.invoice_id,
        'recipient': invoice_info.recipient,
        'status': invoice_info.status, # str, please see models.Invoice.Status
        'created_at': invoice_info.request_time.strftime(timeformat),
        'paid_at': invoice_info.payment_time.strftime(timeformat) if invoice_info.payment_time is not None else None,
        'payment_system_id': invoice_info.payment_system_id,  # 'vinchain' for now, plaase see types.PaymentSystemId
        'requested_asset_id': invoice_info.requested_asset_id,
        'paid_by_asset_id': invoice_info.paid_by_asset_id,
        'requested_amount':  invoice_info.requested_amount,
        'paid_amount': invoice_info.paid_amount,
        'operation_details': invoice_info.operation_details,
        'description': invoice_info.description,
        'expiration_timeout': invoice_info.expiration_timeout.total_seconds(), # timeout of invoice expiration in seconds
        'reserved': invoice_info.reserved,
    })


@csrf_exempt
@require_GET
def get_invoices_info(request):
    pass


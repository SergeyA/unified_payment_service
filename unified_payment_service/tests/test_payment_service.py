import datetime
from unittest.mock import patch, Mock
from functools import partial


from django.test import TestCase
from django.utils import timezone
from django.conf import settings

from unified_payment_service.payment_system_provider import AccountPaymentData
from unified_payment_service.models import Invoice
from unified_payment_service.invoice import InvoiceId
from unified_payment_service.payment_service import create_invoice, get_invoice_info, get_invoices
from unified_payment_service.types import PaymentSystemId



# id = models.BigAutoField(primary_key=True)
# invoice_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
# status = models.CharField(default=Status.REQUESTED, max_length=20, editable=False)
# recipient = models.CharField(max_length=32, editable=False)
# payment_system_id = models.CharField(max_length=32, editable=False)
# asset_id = models.CharField(max_length=32, editable=False)
# requested_amount = models.FloatField(editable=False)
# paid_amount = models.FloatField(editable=False, null=True)
# requester_operation_id = models.CharField(max_length=32, editable=False)
# payer_id = models.CharField(max_length=32, editable=False)
# request_time = models.DateTimeField(editable=False)
# description = models.CharField(max_length=128, editable=False)
# reserved = models.CharField(max_length=128, editable=False, null=True)
# url_success = models.URLField(max_length=2000, editable=False, null=True)
# url_failed = models.URLField(max_length=2000, editable=False, null=True)
# last_checking_time = models.DateTimeField(editable=False, null=True)



class TestPaymentService(TestCase):

    _RECIPIENT = 'sergey-a2'
    _PAYER = 'sergey-a'
    _ASSET_ID = '1.3.0'
    _AMOUNT = 2.34
    _OPERATION_DETAILS = '12234'
    _PAYMENT_TIME = datetime.datetime.now() + datetime.timedelta(minutes=5)
    _EXP_TIMEOUT = datetime.timedelta(days=1)

    def setUp(self):
        self._request_time = datetime.datetime.now(tz=timezone.utc)
        self._invoice_id = create_invoice(recipient=self._RECIPIENT, payment_system_id=PaymentSystemId.VINCHAIN,
                                       asset_id=self._ASSET_ID, amount=self._AMOUNT,
                                       expiration_timeout=self._EXP_TIMEOUT,
                                       operation_details=self._OPERATION_DETAILS,
                                       request_time=self._request_time,
                                       description='Description123', reserved=None,
                                       url_success='https://www.google.com/search?q=pycharm',
                                       url_failed='https://www.google.com/search?q=test',)

        self.assertTrue(InvoiceId.is_valid_invoice_id(self._invoice_id))

    def tearDown(self):
        pass

    # def test_create_invoice(self):
    #     invoice = create_invoice(recipient='sergey-a2', payment_system_id=PaymentSystemId.VINCHAIN,
    #                              asset_id='1.3.0', amount=2.34, requester_operation_id='1234',
    #                              payer_id='432', request_time=datetime.datetime.now(),
    #                              description='Description123', reserved=None,
    #                              url_success='https://www.google.com/search?q=pycharm',
    #                              url_failed='https://www.google.com/search?q=test',)
    #
    #     self.assertTrue(InvoiceId.is_valid_invoice_id(invoice))

    def mock_get_invoice_data_1(account_id, from_date, to_date, invoice_id):
        paymentData = AccountPaymentData(from_=TestPaymentService._PAYER, to=TestPaymentService._RECIPIENT,
                                         amount=TestPaymentService._AMOUNT,
                                         asset_id=TestPaymentService._ASSET_ID,
                                         invoice_id=invoice_id,
                                         operation_time=TestPaymentService._PAYMENT_TIME,
                                         extended_data='')
        return paymentData

    @patch('unified_payment_service.vinchain_payment_system_provider.get_invoice_data',
           side_effect=mock_get_invoice_data_1 )
    def test_get_invoice_info_1(self, *args):
        invoice = get_invoice_info(self._invoice_id, False)

        self.assertEqual(invoice.recipient, self._RECIPIENT)
        self.assertEqual(InvoiceId(invoice.invoice_id), self._invoice_id)
        self.assertEqual(PaymentSystemId(invoice.payment_system_id), PaymentSystemId.VINCHAIN)
        self.assertEqual(invoice.requested_asset_id, self._ASSET_ID)
        self.assertEqual(invoice.requested_amount, self._AMOUNT)
        self.assertEqual(invoice.operation_details, self._OPERATION_DETAILS)
        self.assertEqual(invoice.request_time, self._request_time)
        self.assertEqual(invoice.expiration_timeout, self._EXP_TIMEOUT)

        self.assertEqual(invoice.status, Invoice.Status.PAID)
        self.assertEqual(invoice.paid_amount, self._AMOUNT)
        self.assertEqual(invoice.payment_time, self._PAYMENT_TIME)
        self.assertEqual(invoice.payer, self._PAYER)
        self.assertIsNotNone(invoice.last_checking_time)

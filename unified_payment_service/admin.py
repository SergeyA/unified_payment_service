from django.contrib import admin

from unified_payment_service.models import (
    Invoice,
)

class ReadOnlyMixin:
    def get_actions(self, request):
        actions = super(ReadOnlyMixin, self).get_actions(request)
        del actions["delete_selected"]
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        pass

    def delete_model(self, request, obj):
        pass

    def save_related(self, request, form, formsets, change):
        pass


class InvoicesAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = [field.name for field in Invoice._meta.get_fields()]



admin.site.register(Invoice, InvoicesAdmin)

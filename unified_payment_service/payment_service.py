from datetime import datetime, timedelta, timezone
import typing

from django.db.transaction import atomic

from django.conf import settings
from unified_payment_service.models import Invoice
from unified_payment_service.invoice import InvoiceId
from unified_payment_service.types import (PaymentSystemId, RetrievingPaymentDataError)


def create_invoice(recipient: str, payment_system_id: PaymentSystemId,  asset_id: str, amount: float,
                   operation_details: str, request_time: datetime,
                   description: str, reserved: str, expiration_timeout: timedelta = None,
                   url_success: str = None, url_failed: str = None) -> InvoiceId:
    """
    :param recipient:  recipient id (account id for Vinchain)
    :param payment_system_id: payment system id ('vinchain' for Vinchain)
    :param asset_id: payment asset id (for instance '1.3.0' for Vinchain)
    :param amount:
    :param expiration_timeout: timeout of invoice expiration
    :param operation_details: any details of operations (provided by requester system)
    :param request_time:
    :param description:
    :param reserved:
    :param url_success:
    :param url_failed:
    """
    invoice = Invoice.objects.create(
        recipient=recipient,
        payment_system_id=payment_system_id.value,
        requested_asset_id=asset_id,
        requested_amount=amount,
        expiration_timeout=expiration_timeout,
        operation_details=operation_details,
        request_time=request_time,
        description=description,
        reserved=reserved,
        url_success=url_success,
        url_failed=url_failed,
    )
    return InvoiceId(invoice.invoice_id)


def get_invoice_info(invoice_id: InvoiceId, force: bool=False) -> Invoice:
    invoice = Invoice.objects.get(invoice_id=invoice_id)
    if not invoice:
        return None

    if invoice.last_checking_time is None or invoice.status == Invoice.Status.NOT_PAID_YET or force:
        # need to request payment status from real system
        last_checking_time = datetime.now(timezone.utc)
        invoice.last_checking_time = last_checking_time

        payment_system_id = PaymentSystemId(invoice.payment_system_id)

        provider = _load_provider(payment_system_id)
        if provider is None:
            invoice.status = Invoice.Status.WITHOUT_PROVIDER
            invoice.save()
            return invoice

        try:
            invoice_data = provider.get_invoice_data(invoice.recipient, invoice.request_time,
                                                     invoice.request_time + invoice.expiration_timeout + timedelta(hours=1),
                                                     invoice_id)

            if invoice_data is None:
                # doesn't found
                delta = last_checking_time - invoice.request_time
                if delta > invoice.expiration_timeout:
                    invoice.status = Invoice.Status.EXPIRED
                else:
                    invoice.status = Invoice.Status.NOT_PAID_YET
            else:
                invoice.payer = invoice_data.from_
                invoice.paid_amount = invoice_data.amount
                invoice.paid_by_asset_id = invoice_data.asset_id
                invoice.payment_time = invoice_data.operation_time
                if invoice.paid_by_asset_id != invoice.requested_asset_id:
                    invoice.status = Invoice.Status.PAID_BY_WRONG_ASSET
                else:
                    if invoice.paid_amount == invoice.requested_amount:
                        invoice.status = Invoice.Status.PAID
                    elif invoice.paid_amount > invoice.requested_amount:
                        invoice.status = Invoice.Status.OVERPAID
                    elif invoice.paid_amount < invoice.requested_amount:
                        invoice.status = Invoice.Status.PAID_NOT_FULLY

            invoice.save()
        except RetrievingPaymentDataError:
            return None

    return invoice


def get_invoices_info(recipient: str, payment_system_id: PaymentSystemId=None, asset_id:str=None,
                 requester_operation_id: str=None, payer_id: str=None):
    kwargs = {}
    kwargs['recipient'] = recipient
    if payment_system_id is not None:
        kwargs['payment_system_id'] = payment_system_id.value
    if asset_id is not None:
        kwargs['asset_id'] = asset_id
    if requester_operation_id is not None:
        kwargs['requester_operation_id'] = requester_operation_id
    if payer_id is not None:
        kwargs['payer_id'] = payer_id

    operations = Invoice.objects.filter(**kwargs).order_by('id')
    return operations


def check_sign(payment_system_id, sign, account_id, max_time_diff_sec=5*60):
    provider = _load_provider(payment_system_id)
    if provider is None:
        return False
    return provider.check_sign(sign, account_id, max_time_diff_sec)


def _load_provider(payment_system_id):
    provider_data = settings.PAYMENT_SYSTEMS_PROVIDERS.get(payment_system_id.value)
    if provider_data is None:
        return None

    provider = __import__(provider_data[0], fromlist=[provider_data[1]])
    if not provider:
        return None

    return getattr(provider, provider_data[1])

from django.core.management.base import BaseCommand
from django.conf import settings

from unified_payment_service.vinchain_payment_system_provider import (
    _get_vinchain_account_history,
    _account_name_to_id
)


class Command(BaseCommand):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def add_arguments(self, parser):
        parser.add_argument(
            '--test_type', type=str, help='Type of test (string).', default=0
        )

        super(Command, self).add_arguments(parser)

    def handle(self, *app_labels, **options):
        test_type = options['test_type']
        if test_type == 'get_vinchain_account_history':
            es_connect_args = {'hosts': [settings.VINCHAIN_ELASTICSEARCH_NODE], 'timeout': 60}
            result = _get_vinchain_account_history(es_connect_args, '1.2.5041', '0', '2019-04-01T00:00:00', '2019-04-10T00:00:00')
            for i in result:
                print(i)

        elif test_type == '_account_name_to_id':
            result = _account_name_to_id('sergey-a')
            print(result)
            result = _account_name_to_id('sergey-a')
            print(result)
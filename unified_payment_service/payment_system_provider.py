from collections import namedtuple


AccountPaymentData = namedtuple('AccountPaymentData', ['from_', 'to', 'amount', 'asset_id', 'invoice_id',
                                                       'operation_time', 'extended_data'])


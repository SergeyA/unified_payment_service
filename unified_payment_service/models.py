import uuid
from datetime import timedelta

from django.db import models
from django.utils.translation import ugettext_lazy as _



class Invoice(models.Model):
    class Status:
        REQUESTED = 'requested'
        PAID = 'paid'
        NOT_PAID_YET = 'not_paid_yet'
        PAID_NOT_FULLY = 'paid_not_fully'
        OVERPAID = 'overpaid'
        PAID_BY_WRONG_ASSET = 'paid_by_wrong_asset'
        WITHOUT_PROVIDER = 'without_provider'
        EXPIRED = 'expired'


    id = models.BigAutoField(primary_key=True)
    invoice_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    status = models.CharField(default=Status.REQUESTED, max_length=20, editable=False)
    recipient = models.CharField(max_length=32, editable=False)
    payer = models.CharField(max_length=32, editable=False, null=True)
    payment_system_id = models.CharField(max_length=32, editable=False)
    requested_asset_id = models.CharField(max_length=32, editable=False)
    paid_by_asset_id = models.CharField(max_length=32, editable=False, null=True)
    requested_amount = models.FloatField(editable=False)
    paid_amount = models.FloatField(editable=False, null=True)
    expiration_timeout = models.DurationField(editable=False, default=timedelta(days=1))
    operation_details = models.CharField(max_length=256, editable=False)
    request_time = models.DateTimeField(editable=False)
    payment_time = models.DateTimeField(editable=False, null=True)
    description = models.CharField(max_length=128, editable=False)
    reserved = models.CharField(max_length=128, editable=False, null=True)
    url_success = models.URLField(max_length=2000, editable=False, null=True)
    url_failed = models.URLField(max_length=2000, editable=False, null=True)
    last_checking_time = models.DateTimeField(editable=False, null=True)


    class Meta:
        ordering = ['id']
        indexes = [
            models.Index(fields=['invoice_id',]),
        ]

    def __str__(self):
        return '{}_{}_{}_{}_{}_{}_{}_{}_{}'.format(self.invoice_id,
                                                   self.recipient,
                                                   self.payment_system_id,
                                                   self.requested_asset_id,
                                                   self.paid_by_asset_id,
                                                   self.requested_amount,
                                                   self.paid_amount,
                                                   self.request_time,
                                                   self.status)
